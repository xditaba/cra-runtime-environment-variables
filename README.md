## CRA Runtime Environment Variables

Reference: https://www.freecodecamp.org/news/how-to-implement-runtime-environment-variables-with-create-react-app-docker-and-nginx-7f9d42a91d70/

### Print executed commands when run bash script

```
$ sh -x env.sh
```

Some pictures note

![Pic1](./src/images/env-variables-system.PNG)

---

![Pic2](./src/images/echo-in-env-script.PNG)

---

![Pic3](./src/images/printout-terminal-console.PNG)

### Run command with override API_URL environment variable

```
$ API_URL=https://alibaba.com yarn dev
```

Output:

```
$ API_URL=https://alibaba.com yarn dev
yarn run v1.17.3
$ chmod +x ./env.sh && sh -x env.sh >> test.txt && cp env-config.js ./public/
+ rm -rf ./env-config.js
+ touch ./env-config.js
+ echo di.ta
+ echo 'window._env_ = {'
+ read -r line
+ grep -q -e =
++ printf '%s\n' API_URL=https://default2.dev.api.com
++ sed -e 's/=.*//'
+ varname=API_URL
++ printf '%s\n' API_URL=https://default2.dev.api.com
++ sed -e 's/^[^=]*=//'
+ varvalue=https://default2.dev.api.com
++ printf '%s\n' https://alibaba.com
+ value=https://alibaba.com
+ [[ -z https://alibaba.com ]]
+ echo '  API_URL: "https://alibaba.com",'
+ read -r line
+ [[ -n '' ]]
+ echo '}'
Done in 0.38s.
```

## Add `bash -x` in dockerfile file to print executed commands

```
CMD ["/bin/bash", "-c", "bash -x /usr/share/nginx/html/env.sh && nginx -g \"daemon off;\""]
```

Output:

```
$ docker-compose up
Creating cra-runtime-environment-variables_cra-runtime-environment-variables_1 ... done
Attaching to cra-runtime-environment-variables_cra-runtime-environment-variables_1
cra-runtime-environment-variables_1  | + rm -rf ./env-config.js
cra-runtime-environment-variables_1  | + touch ./env-config.js
cra-runtime-environment-variables_1  | + echo
cra-runtime-environment-variables_1  | + echo 'window._env_ = {'
cra-runtime-environment-variables_1  | + read -r line
cra-runtime-environment-variables_1  | + printf '%s\n' $'API_URL=https://default.dev.api.com\r'
cra-runtime-environment-variables_1  | + grep -q -e =
cra-runtime-environment-variables_1  | ++ printf '%s\n' $'API_URL=https://default.dev.api.com\r'
cra-runtime-environment-variables_1  | ++ sed -e 's/=.*//'
cra-runtime-environment-variables_1  | + varname=API_URL
cra-runtime-environment-variables_1  |
cra-runtime-environment-variables_1  | ++ printf '%s\n' $'API_URL=https://default.dev.api.com\r'
cra-runtime-environment-variables_1  | ++ sed -e 's/^[^=]*=//'
cra-runtime-environment-variables_1  | + varvalue=$'https://default.dev.api.com\r'
cra-runtime-environment-variables_1  | ++ printf '%s\n' production.example.com
cra-runtime-environment-variables_1  | + value=production.example.com
cra-runtime-environment-variables_1  | + [[ -z production.example.com ]]
cra-runtime-environment-variables_1  | + echo '  API_URL: "production.example.com",'
cra-runtime-environment-variables_1  | + read -r line
cra-runtime-environment-variables_1  | + [[ -n '' ]]
cra-runtime-environment-variables_1  | + echo '}'
```
